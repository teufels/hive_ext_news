
plugin.tx_hiveextnews_hiveextnewsnewslist {
    view {
        # cat=plugin.tx_hiveextnews_hiveextnewsnewslist/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_news/Resources/Private/Templates/
        # cat=plugin.tx_hiveextnews_hiveextnewsnewslist/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_news/Resources/Private/Partials/
        # cat=plugin.tx_hiveextnews_hiveextnewsnewslist/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_news/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveextnews_hiveextnewsnewslist//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_hiveextnews_hiveextnewsnewsdetail {
    view {
        # cat=plugin.tx_hiveextnews_hiveextnewsnewsdetail/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_news/Resources/Private/Templates/
        # cat=plugin.tx_hiveextnews_hiveextnewsnewsdetail/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_news/Resources/Private/Partials/
        # cat=plugin.tx_hiveextnews_hiveextnewsnewsdetail/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_news/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveextnews_hiveextnewsnewsdetail//a; type=string; label=Default storage PID
        storagePid =
    }
}
