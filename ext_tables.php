<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveExtNews',
            'Hiveextnewsnewslist',
            'hive_ext_news :: News :: list'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveExtNews',
            'Hiveextnewsnewsdetail',
            'hive_ext_news :: News :: Detail'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_ext_news', 'Configuration/TypoScript', 'hive_ext_news');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextnews_domain_model_news', 'EXT:hive_ext_news/Resources/Private/Language/locallang_csh_tx_hiveextnews_domain_model_news.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextnews_domain_model_news');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
            'hive_ext_news',
            'tx_hiveextnews_domain_model_news'
        );

    }
);
