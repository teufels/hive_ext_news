<?php
namespace HIVE\HiveExtNews\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Andreas Hafner <a.hafner@teufels.com>
 * @author Dominik Hilser <d.hilser@teufels.com>
 * @author Georg Kathan <g.kathan@teufels.com>
 * @author Hendrik Krüger <h.krueger@teufels.com>
 * @author Josymar Escalona Rodriguez <j.rodriguez@teufels.com>
 * @author Perrin Ennen <p.ennen@teufels.com>
 * @author Timo Bittner <t.bittner@teufels.com>
 * @author Yannick Aister <y.aister@teufels.com>
 */
class NewsTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtNews\Domain\Model\News
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtNews\Domain\Model\News();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getHeaderReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getHeader()
        );
    }

    /**
     * @test
     */
    public function setHeaderForStringSetsHeader()
    {
        $this->subject->setHeader('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'header',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFeaturedNewsReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getFeaturedNews()
        );
    }

    /**
     * @test
     */
    public function setFeaturedNewsForBoolSetsFeaturedNews()
    {
        $this->subject->setFeaturedNews(true);

        self::assertAttributeEquals(
            true,
            'featuredNews',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTeaserReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTeaser()
        );
    }

    /**
     * @test
     */
    public function setTeaserForStringSetsTeaser()
    {
        $this->subject->setTeaser('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'teaser',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAuthorNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getAuthorName()
        );
    }

    /**
     * @test
     */
    public function setAuthorNameForStringSetsAuthorName()
    {
        $this->subject->setAuthorName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'authorName',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAuthorEmailReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getAuthorEmail()
        );
    }

    /**
     * @test
     */
    public function setAuthorEmailForStringSetsAuthorEmail()
    {
        $this->subject->setAuthorEmail('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'authorEmail',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAuthorAdressReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getAuthorAdress()
        );
    }

    /**
     * @test
     */
    public function setAuthorAdressForStringSetsAuthorAdress()
    {
        $this->subject->setAuthorAdress('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'authorAdress',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAuthorImgReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getAuthorImg()
        );
    }

    /**
     * @test
     */
    public function setAuthorImgForFileReferenceSetsAuthorImg()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setAuthorImg($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'authorImg',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDateReturnsInitialValueForDateTime()
    {
        self::assertEquals(
            null,
            $this->subject->getDate()
        );
    }

    /**
     * @test
     */
    public function setDateForDateTimeSetsDate()
    {
        $dateTimeFixture = new \DateTime();
        $this->subject->setDate($dateTimeFixture);

        self::assertAttributeEquals(
            $dateTimeFixture,
            'date',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getContentReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getContent()
        );
    }

    /**
     * @test
     */
    public function setContentForStringSetsContent()
    {
        $this->subject->setContent('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'content',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getThumbnailReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getThumbnail()
        );
    }

    /**
     * @test
     */
    public function setThumbnailForFileReferenceSetsThumbnail()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setThumbnail($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'thumbnail',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getContentImgReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getContentImg()
        );
    }

    /**
     * @test
     */
    public function setContentImgForFileReferenceSetsContentImg()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setContentImg($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'contentImg',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getContentImgCaptionReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getContentImgCaption()
        );
    }

    /**
     * @test
     */
    public function setContentImgCaptionForStringSetsContentImgCaption()
    {
        $this->subject->setContentImgCaption('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'contentImgCaption',
            $this->subject
        );
    }
}
