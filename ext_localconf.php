<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtNews',
            'Hiveextnewsnewslist',
            [
                'News' => 'list, show'
            ],
            // non-cacheable actions
            [
                'News' => 'list, show'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtNews',
            'Hiveextnewsnewsdetail',
            [
                'News' => 'show'
            ],
            // non-cacheable actions
            [
                'News' => 'show'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hiveextnewsnewslist {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_news') . 'Resources/Public/Icons/user_plugin_hiveextnewsnewslist.svg
                        title = LLL:EXT:hive_ext_news/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_news_domain_model_hiveextnewsnewslist
                        description = LLL:EXT:hive_ext_news/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_news_domain_model_hiveextnewsnewslist.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveextnews_hiveextnewsnewslist
                        }
                    }
                    hiveextnewsnewsdetail {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_news') . 'Resources/Public/Icons/user_plugin_hiveextnewsnewsdetail.svg
                        title = LLL:EXT:hive_ext_news/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_news_domain_model_hiveextnewsnewsdetail
                        description = LLL:EXT:hive_ext_news/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_news_domain_model_hiveextnewsnewsdetail.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveextnews_hiveextnewsnewsdetail
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
